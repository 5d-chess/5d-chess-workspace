# 5D Chess Workspace

Unified workspace to work on 5D Chess projects simultaneously

## Installation

Use the `setup.bat` script to automatically setup git repositories

Chess in 5D's master branch is broken, so the `setup.bat` script will automatically checkout to the `feature-ui_overhaul` branch

## Usage

Run `npm start` before any other npm commands in sub directories in order to propagate local changes as modules.

Supported repositories:
 - 5d-chess-js (changes under npm start will trigger propagation)
 - 5d-chess-renderer (changes under npm start will trigger propagation, html files will pull from locally propagated 5d-chess-js module)
 - chess-in-5d (changes to 5d-chess-js or 5d-chess-renderer will trigger recompile with local module)
 - 5d-chess-server (changes to 5d-chess-js or 5d-chess-renderer will require manual restart to utilize local module)

To undo all changes, stop any existing npm commands in sub directories before exiting the main `npm start` process (since this will trigger reverting local module redirection).

Run `npm revert` if the revert process is not automatically triggered on exit of `npm start` process.