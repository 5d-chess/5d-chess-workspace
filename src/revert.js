const fs = require('fs');
const path = require('path');

//Remove 5d-chess-js files from chess-in-5d
try { fs.copyFileSync(path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-js/dist/5d-chess.js.tmp'), path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-js/dist/5d-chess.js')); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-js/dist/5d-chess.js.tmp'), { force: true }); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-js/5d-chess.js'), { force: true }); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-js/package.json'), { force: true }); } catch(err) {}

//Remove 5d-chess-renderer files from chess-in-5d
try { fs.copyFileSync(path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-renderer/dist/5d-chess-renderer.js.tmp'), path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-renderer/dist/5d-chess-renderer.js')); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-renderer/dist/5d-chess-renderer.js.tmp'), { force: true }); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-renderer/5d-chess-renderer.js'), { force: true }); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-renderer/package.json'), { force: true }); } catch(err) {}

//Remove 5d-chess-js files from 5d-chess-renderer
try { fs.rmSync(path.resolve(__dirname, '../5d-chess-renderer/.5d-chess-workspace/5d-chess.js'), { force: true }); } catch(err) {}

//Remove 5d-chess-js files from 5d-chess-server
try { fs.copyFileSync(path.resolve(__dirname, '../5d-chess-server/node_modules/5d-chess-js/dist/5d-chess.js.tmp'), path.resolve(__dirname, '../5d-chess-server/node_modules/5d-chess-js/dist/5d-chess.js')); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../5d-chess-server/node_modules/5d-chess-js/dist/5d-chess.js.tmp'), { force: true }); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../5d-chess-server/.5d-chess-workspace/5d-chess-js/5d-chess.js'), { force: true }); } catch(err) {}
try { fs.rmSync(path.resolve(__dirname, '../5d-chess-server/.5d-chess-workspace/5d-chess-js/package.json'), { force: true }); } catch(err) {}

//Remove .5d-chess-workspace sub directories
try { fs.rmdirSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-js')); } catch(err) {}
try { fs.rmdirSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-renderer')); } catch(err) {}
try { fs.rmdirSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace')); } catch(err) {}
try { fs.rmdirSync(path.resolve(__dirname, '../5d-chess-renderer/.5d-chess-workspace')); } catch(err) {}
try { fs.rmdirSync(path.resolve(__dirname, '../5d-chess-server/.5d-chess-workspace/5d-chess-js')); } catch(err) {}
try { fs.rmdirSync(path.resolve(__dirname, '../5d-chess-server/.5d-chess-workspace')); } catch(err) {}