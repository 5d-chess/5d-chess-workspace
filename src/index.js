const fs = require('fs');
const path = require('path');
const process = require('process');
const { debounce } = require('throttle-debounce');

//Create .5d-chess-workspace sub directories
try { fs.mkdirSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-js'), { recursive: true}); } catch(err) {}
try { fs.mkdirSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-renderer'), { recursive: true}); } catch(err) {}
try { fs.mkdirSync(path.resolve(__dirname, '../5d-chess-renderer/.5d-chess-workspace')); } catch(err) {}
try { fs.mkdirSync(path.resolve(__dirname, '../5d-chess-server/.5d-chess-workspace/5d-chess-js'), { recursive: true}); } catch(err) {}

const copy5dChessJs = () => {
  let packageJson = {
    version: '9999.9999.9999',
    name: '5d-chess-js',
    main: './5d-chess.js'
  };
  try {
    const chessin5dpkg = require(path.resolve(__dirname, '../chess-in-5d/package.json'));
    packageJson.version = chessin5dpkg.dependencies['5d-chess-js'].substr(1);
  }
  catch(err) {}
  try { fs.writeFileSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-js/package.json'), JSON.stringify(packageJson)); } catch(err) {}
  try { fs.copyFileSync(path.resolve(__dirname, '../5d-chess-js/dist/5d-chess.js'), path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-js/5d-chess.js')); } catch(err) {}
  //Override node_modules version (chess-in-5d)
  try { fs.copyFileSync(path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-js/dist/5d-chess.js'), path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-js/dist/5d-chess.js.tmp')); } catch(err) {}
  try { fs.rmSync(path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-js/dist/5d-chess.js'), { force: true }); } catch(err) {}
  try { fs.copyFileSync(path.resolve(__dirname, '../5d-chess-js/dist/5d-chess.js'), path.resolve(__dirname, '../5d-chess-renderer/.5d-chess-workspace/5d-chess.js')); } catch(err) {}
  try {
    const chessin5dpkg = require(path.resolve(__dirname, '../5d-chess-server/package.json'));
    packageJson.version = chessin5dpkg.dependencies['5d-chess-js'].substr(1);
  }
  catch(err) {}
  try { fs.writeFileSync(path.resolve(__dirname, '../5d-chess-server/.5d-chess-workspace/5d-chess-js/package.json'), JSON.stringify(packageJson)); } catch(err) {}
  try { fs.copyFileSync(path.resolve(__dirname, '../5d-chess-js/dist/5d-chess.js'), path.resolve(__dirname, '../5d-chess-server/.5d-chess-workspace/5d-chess-js/5d-chess.js')); } catch(err) {}
  //Override node_modules version (5d-chess-server)
  try { fs.copyFileSync(path.resolve(__dirname, '../5d-chess-server/node_modules/5d-chess-js/dist/5d-chess.js'), path.resolve(__dirname, '../5d-chess-server/node_modules/5d-chess-js/dist/5d-chess.js.tmp')); } catch(err) {}
  try { fs.rmSync(path.resolve(__dirname, '../5d-chess-server/node_modules/5d-chess-js/dist/5d-chess.js'), { force: true }); } catch(err) {}
}

//Watch for 5d-chess-js changes and copy to workspace
fs.watch(path.resolve(__dirname, '../5d-chess-js/dist/5d-chess.js'), {}, debounce(500, (eventType, filename) => {
  if(eventType === 'change' && filename) {
    console.info('Detected change in 5d-chess-js, copying to .5d-chess-workspace');
    copy5dChessJs();
  }
}));

const copy5dChessRenderer = () => {
  let packageJson = {
    version: '9999.9999.9999',
    name: '5d-chess-renderer',
    main: './5d-chess-renderer.js'
  };
  try {
    const chessin5dpkg = require(path.resolve(__dirname, '../chess-in-5d/package.json'));
    packageJson.version = chessin5dpkg.dependencies['5d-chess-renderer'].substr(1);
  }
  catch(err) {}
  try { fs.writeFileSync(path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-renderer/package.json'), JSON.stringify(packageJson)); } catch(err) {}
  try { fs.copyFileSync(path.resolve(__dirname, '../5d-chess-renderer/dist/5d-chess-renderer.js'), path.resolve(__dirname, '../chess-in-5d/src/.5d-chess-workspace/5d-chess-renderer/5d-chess-renderer.js')); } catch(err) {}
  //Override node_modules version
  try { fs.copyFileSync(path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-renderer/dist/5d-chess-renderer.js'), path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-renderer/dist/5d-chess-renderer.js.tmp')); } catch(err) {}
  try { fs.rmSync(path.resolve(__dirname, '../chess-in-5d/node_modules/5d-chess-renderer/dist/5d-chess-renderer.js'), { force: true }); } catch(err) {}
}

//Watch for 5d-chess-renderer changes and copy to workspace
fs.watch(path.resolve(__dirname, '../5d-chess-renderer/dist/5d-chess-renderer.js'), {}, debounce(500, (eventType, filename) => {
  if(eventType === 'change' && filename) {
    console.info('Detected change in 5d-chess-renderer, copying to .5d-chess-workspace');
    copy5dChessRenderer();
  }
}));

console.info('Listening to changes in 5d-chess-js and 5d-chess-renderer, copying to .5d-chess-workspace');
copy5dChessJs();
copy5dChessRenderer();

const exitHandler = (options) => {
  if(options.exit) {
    console.info('Reverting all changes before exiting');
    require('./revert');
    process.exit();
  }
}
process.on('exit', exitHandler.bind(this, { exit: false }));
process.on('SIGINT', exitHandler.bind(this, { exit: true }));