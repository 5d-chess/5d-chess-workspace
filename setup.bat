@ECHO OFF

CALL npm install

IF NOT EXIST 5d-chess-clock\ (
  ECHO npm install on 5d-chess-clock
  CALL git clone https://gitlab.com/5d-chess/5d-chess-clock.git
  CD 5d-chess-clock
  CALL npm install
  CD ..
)

IF NOT EXIST 5d-chess-clock\ (
  ECHO npm install on 5d-chess-db
  CALL git clone https://gitlab.com/5d-chess/5d-chess-db.git
  CD 5d-chess-db
  CALL npm install
  CD ..
)

IF NOT EXIST 5d-chess-discord\ (
  ECHO npm install on 5d-chess-discord
  CALL git clone https://gitlab.com/5d-chess/5d-chess-discord.git
  CD 5d-chess-discord
  CALL npm install
  CD ..
)

IF NOT EXIST 5d-chess-js\ (
  ECHO npm install on 5d-chess-js
  CALL git clone https://gitlab.com/5d-chess/5d-chess-js.git
  CD 5d-chess-js
  CALL npm install
  CD ..
)

IF NOT EXIST 5d-chess-ts\ (
  ECHO npm install on 5d-chess-ts
  CALL git clone https://gitlab.com/5d-chess/5d-chess-ts.git
  CD 5d-chess-ts
  CALL npm install
  CD ..
)

IF NOT EXIST 5d-chess-renderer\ (
  ECHO npm install on 5d-chess-renderer
  CALL git clone https://gitlab.com/5d-chess/5d-chess-renderer.git
  CD 5d-chess-renderer
  CALL npm install
  CD ..
)

IF NOT EXIST 5d-chess-server\ (
  ECHO npm install on 5d-chess-server
  CALL git clone https://gitlab.com/5d-chess/5d-chess-server.git
  CD 5d-chess-server
  CALL npm install
  CD ..
)

IF NOT EXIST chess-in-5d\ (
  ECHO npm install on chess-in-5d
  CALL git clone https://gitlab.com/5d-chess/chess-in-5d.git
  CD chess-in-5d
  CALL npm install
  CD ..
)

PAUSE
